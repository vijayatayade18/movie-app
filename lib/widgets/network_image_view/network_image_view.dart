import 'dart:io';

import 'package:flutter/material.dart';
import 'package:movie_app/utils/download_util.dart';

typedef LoadedImageBuilder = Widget Function(File);

class NetworkImageView extends StatelessWidget {
  final String url;
  final Widget placeholderWidget;
  final LoadedImageBuilder imageBuilder;
  final bool useAnimation;

  NetworkImageView(
    this.url, {
    this.placeholderWidget,
    this.imageBuilder,
    this.useAnimation = true,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<File>(
      future: DownloadUtil.download(url),
      builder: (context, snapshot) {
        if (!useAnimation) {
          return _getChild(snapshot);
        }
        return AnimatedContainer(
          key: ObjectKey(snapshot.data),
          duration: Duration(milliseconds: 200),
          child: _getChild(snapshot),
        );
      },
    );
  }

  Widget _getChild(AsyncSnapshot<File> snapshot) {
    if (snapshot.hasData) {
      File file = snapshot.data;
      return (imageBuilder != null)
          ? imageBuilder(file)
          : Image.file(
              file,
              alignment: Alignment.center,
              fit: BoxFit.fill,
              height: 200,
              width: 120,
            );
    } else {
      return placeholderWidget;
    }
  }
}

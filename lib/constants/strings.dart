class AppStrings {
  static String movie = "Movie";
  static String releaseDateLabel = "Release Date";
  static String languageLabel = "Language";
  static String ratingLabel = "Rating";
  static String errorMessage="Something went wrong, please try again later.";
  static String noMoviesFound="No movies found";
}

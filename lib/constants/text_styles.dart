import 'package:flutter/material.dart';

class TextStyles {
  static const String poppins = 'Poppins';

  static const FontWeight light = FontWeight.w300;
  static const FontWeight regular = FontWeight.w400;
  static const FontWeight medium = FontWeight.w500;
  static const FontWeight bold = FontWeight.w700;
  static const FontWeight semiBold = FontWeight.w600;

  static TextStyle get movieCardTextStyle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 16.0,
      );

  static TextStyle get movieCardSubTitleStyle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: regular,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get movieNameStyle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: bold,
        fontFamily: poppins,
        fontSize: 30.0,
      );

  static TextStyle get movieOverviewStyle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 18.0,
      );

  static TextStyle get errorTextStyle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: regular,
        fontFamily: poppins,
        fontSize: 16.0,
      );

  static TextStyle get retryButtonTextStyle => TextStyle(
        inherit: false,
        color: Colors.black,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 12.0,
      );

  static TextStyle get appBarTitle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 18.0,
      );
}

class Assets {
  static String gridIcon = 'assets/images/ic_grid.png';
  static String listIcon = 'assets/images/ic_list.png';
  static String releaseDate = 'assets/images/release_date.png';
  static String language = 'assets/images/language.png';
  static String rating = 'assets/images/rating.png';
  static String moviePosterPlaceHolder = 'assets/images/movie_banner_placeholder.png';
  static String error = 'assets/images/error.png';
}

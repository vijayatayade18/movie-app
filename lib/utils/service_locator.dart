import 'package:get_it/get_it.dart';
import 'package:movie_app/data/local/db/my_db.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  locator.registerSingleton(MyDatabase());
}
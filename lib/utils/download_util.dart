import 'dart:io';

import 'package:movie_app/utils/movie_cache_manager.dart';

class DownloadUtil {
  static Future<File> download(String url) async {
    File cachedFile = (await MovieCacheManager().getFileFromCache(url))?.file;
    if (cachedFile == null) {
      cachedFile = (await MovieCacheManager().downloadFile(url))?.file;
    }
    return cachedFile;
  }
}

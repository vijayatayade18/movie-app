import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class MovieCacheManager extends BaseCacheManager {
  static const key = "movieCache";

  static MovieCacheManager _instance;

  factory MovieCacheManager() {
    if (_instance == null) {
      _instance = MovieCacheManager._();
    }
    return _instance;
  }

  MovieCacheManager._() : super(key, maxAgeCacheObject: Duration(days: 7), maxNrOfCacheObjects: 20);

  @override
  Future<String> getFilePath() async {
    var directory = await getTemporaryDirectory();
    return join(directory.path, key);
  }
}

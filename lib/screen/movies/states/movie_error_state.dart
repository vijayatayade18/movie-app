import 'package:flutter/material.dart';
import 'package:movie_app/constants/assets.dart';
import 'package:movie_app/constants/text_styles.dart';
import 'package:movie_app/screen/movies/states/index.dart';
import 'package:movie_app/screen/movies/states/movie_states.dart';
import 'package:movie_app/utils/index.dart';

class MovieErrorState extends StatefulWidget implements MovieState {
  final String errorMessage;
  final String icon;
  final bool showRetryButton;
  final VoidCallback onRetryClicked;

  const MovieErrorState({
    Key key,
    this.errorMessage,
    this.icon,
    this.showRetryButton = true,
    this.onRetryClicked,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MovieErrorState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _MovieErrorState extends State<MovieErrorState> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: Color(0xFF26272C),
      padding: EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 12,
      ),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: UIUtil.getSeparatedWidgets(
            [
              Image.asset(
                widget.icon ?? Assets.error,
                height: 70,
                width: 70,
                color: Colors.white,
              ),
              Text(
                widget.errorMessage ?? "Something went wrong, please try again later.",
                textAlign: TextAlign.center,
                style: TextStyles.errorTextStyle,
                maxLines: 3,
              ),
              widget.showRetryButton
                  ? RaisedButton(
                      onPressed: () {
                        debugPrint("Retry clicked");
                        if (widget.onRetryClicked != null) {
                          widget.onRetryClicked();
                        }
                      },
                      child: Text(
                        "Retry",
                        textAlign: TextAlign.center,
                        style: TextStyles.retryButtonTextStyle,
                      ),
                    )
                  : SizedBox.shrink(),
            ],
            interItemSpace: 16,
            flowHorizontal: false,
          )),
    );
  }
}

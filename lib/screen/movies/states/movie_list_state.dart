import 'package:flutter/material.dart';
import 'package:movie_app/data/models/index.dart';
import 'package:movie_app/screen/movies/index.dart';
import 'package:movie_app/screen/movies/states/index.dart';
import 'package:movie_app/screen/movies/widgets/index.dart';
import 'package:movie_app/widgets/network_image_view/index.dart';

class MovieListState extends StatefulWidget implements MovieState {
  final List<MovieDetailsDTO> moviesList;

  MovieListState(this.moviesList);

  @override
  State<StatefulWidget> createState() => _MovieListState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _MovieListState extends State<MovieListState> {
  LoadedImageBuilder widgetBuilder;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint("length: ${widget.moviesList.length}");
    return ListView.separated(
      padding: EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 12,
      ),
      itemCount: widget.moviesList.length,
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(
          height: 10,
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return MovieListCard(
          movieDetailsDTO: widget.moviesList[index],
          onMovieCardClick: () {
            _navigateToMovieDetailsScreen(widget.moviesList[index]);
          },
          widgetBuilder: widgetBuilder,
        );
      },
    );
  }

  Future _navigateToMovieDetailsScreen(MovieDetailsDTO movieDetailsDTO) {
    return Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => MovieDetailsScreen(
          movieDetails: movieDetailsDTO,
        ),
        transitionDuration: Duration(milliseconds: 500),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var begin = Offset(0.0, 1.0);
          var end = Offset.zero;
          var curve = Curves.ease;
          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
      ),
    );
  }
}

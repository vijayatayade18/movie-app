import 'package:flutter/material.dart';
import 'package:movie_app/constants/index.dart';
import 'package:movie_app/screen/movies/states/index.dart';
import 'package:movie_app/utils/index.dart';
import 'package:movie_app/widgets/custom_card/index.dart';
import 'package:movie_app/widgets/shimmer/index.dart';

class MovieLoadingState extends StatefulWidget implements MovieState {
  @override
  State<StatefulWidget> createState() => _MovieLoadingState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _MovieLoadingState extends State<MovieLoadingState> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 12,
      ),
      child: Column(
          children: UIUtil.getSeparatedWidgets(
        [
          _getMovieCard(),
          _getMovieCard(),
        ],
        interItemSpace: 10,
        flowHorizontal: false,
      )),
    );
  }

  Widget _getMovieCard() {
    return Shimmer(
      child: CustomCard(
        backgroundColor: Color(0xff3f414a).withOpacity(0.2),
        child: Container(
          padding: EdgeInsets.all(
            12.0,
          ),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: UIUtil.getSeparatedWidgets(
                [
                  CustomCard(
                      child: Container(
                    padding: EdgeInsets.all(
                      10.0,
                    ),
                    child: Image.asset(
                      Assets.moviePosterPlaceHolder,
                      fit: BoxFit.fill,
                      height: 100,
                      width: 80,
                      color: AppColors.bgColor,
                    ),
                  )),
                  Expanded(
                    child: Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: UIUtil.getSeparatedWidgets(
                            [
                              _getShimmerContainer(),
                              _getShimmerContainer(),
                              _getShimmerContainer(),
                            ],
                            interItemSpace: 12,
                            flowHorizontal: false,
                          )),
                    ),
                  ),
                ],
                interItemSpace: 12,
              )),
        ),
      ),
    );
  }

  Container _getShimmerContainer() {
    return Container(
      width: 120,
      height: 10,
      color: AppColors.shimmerColor,
    );
  }
}

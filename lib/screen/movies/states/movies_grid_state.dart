import 'package:flutter/material.dart';
import 'package:movie_app/data/models/index.dart';
import 'package:movie_app/screen/movies/index.dart';
import 'package:movie_app/screen/movies/states/index.dart';
import 'package:movie_app/screen/movies/widgets/index.dart';
import 'package:movie_app/widgets/network_image_view/index.dart';

class MoviesGridState extends StatefulWidget implements MovieState {
  final List<MovieDetailsDTO> moviesList;

  MoviesGridState(this.moviesList);

  @override
  State<StatefulWidget> createState() => _MoviesGridState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _MoviesGridState extends State<MoviesGridState> {
  LoadedImageBuilder widgetBuilder;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 12,
      ),
      child: GridView.count(
        crossAxisCount: 2,
        crossAxisSpacing: 14,
        children: List.generate(
          widget.moviesList.length,
          (index) {
            return MovieGridCard(
              movieDetailsDTO: widget.moviesList[index],
              widgetBuilder: widgetBuilder,
              onMovieCardClick: () {
                _navigateToMovieDetailsScreen(widget.moviesList[index]);
              },
            );
          },
          growable: true,
        ),
      ),
    );
  }

  void _navigateToMovieDetailsScreen(
    MovieDetailsDTO movieDetailsDTO,
  ) {
    Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => MovieDetailsScreen(
          movieDetails: movieDetailsDTO,
        ),
        transitionDuration: Duration(milliseconds: 500),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var begin = Offset(0.0, 1.0);
          var end = Offset.zero;
          var curve = Curves.ease;
          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
      ),
    );
  }
}

import 'package:equatable/equatable.dart';

abstract class MovieEvents extends Equatable {}

class LoadingStateEvent implements MovieEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class MoviesListStateEvent implements MovieEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class MoviesGridStateEvent implements MovieEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class MoviesErrorStateEvent implements MovieEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

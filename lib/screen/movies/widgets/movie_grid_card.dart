import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/constants/index.dart';
import 'package:movie_app/data/models/index.dart';
import 'package:movie_app/widgets/custom_card/index.dart';
import 'package:movie_app/widgets/network_image_view/index.dart';

class MovieGridCard extends StatelessWidget {
  final VoidCallback onMovieCardClick;
  final MovieDetailsDTO movieDetailsDTO;
  final LoadedImageBuilder widgetBuilder;

  const MovieGridCard({
    Key key,
    this.onMovieCardClick,
    this.movieDetailsDTO,
    this.widgetBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _getNetworkImage();
  }

  Widget _getNetworkImage() {
    return GestureDetector(
      onTap: () {
        if (onMovieCardClick != null) {
          onMovieCardClick();
        }
      },
      child: Container(
        padding: EdgeInsets.only(
          bottom: 14,
        ),
        child: CustomCard(
          child: Container(
            child: NetworkImageView(
              movieDetailsDTO.imagePath,
              placeholderWidget: Container(
                padding: EdgeInsets.all(10),
                child: Image.asset(
                  Assets.moviePosterPlaceHolder,
                  fit: BoxFit.fill,
                  color: AppColors.bgColor,
                ),
              ),
              imageBuilder: (file) => _getLoadedImage(file),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getLoadedImage(File file) {
    if (widgetBuilder == null) {
      return Image.file(
        file,
        fit: BoxFit.fill,
      );
    } else {
      return widgetBuilder(file);
    }
  }
}

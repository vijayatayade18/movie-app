import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/constants/index.dart';
import 'package:movie_app/data/models/index.dart';
import 'package:movie_app/utils/index.dart';
import 'package:movie_app/widgets/custom_card/index.dart';
import 'package:movie_app/widgets/network_image_view/index.dart';

class MovieListCard extends StatelessWidget {
  final VoidCallback onMovieCardClick;
  final MovieDetailsDTO movieDetailsDTO;
  final LoadedImageBuilder widgetBuilder;

  const MovieListCard({
    Key key,
    this.onMovieCardClick,
    this.movieDetailsDTO,
    this.widgetBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onMovieCardClick != null) {
          onMovieCardClick();
        }
      },
      child: CustomCard(
        backgroundColor: Color(0xff3f414a).withOpacity(0.2),
        child: Container(
          padding: EdgeInsets.all(
            12.0,
          ),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: UIUtil.getSeparatedWidgets(
                [
                  CustomCard(
                    child: Container(
                      child: _getNetworkImage(
                        movieDetailsDTO.imagePath,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: UIUtil.getSeparatedWidgets(
                            [
                              Text(
                                "${movieDetailsDTO.title}",
                                style: TextStyles.movieCardTextStyle,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              _getDetailView(
                                icon: Assets.releaseDate,
                                text: movieDetailsDTO.releaseDate,
                              ),
                              _getDetailView(
                                icon: Assets.language,
                                text: movieDetailsDTO.originalLanguage,
                              ),
                              _getDetailView(
                                icon: Assets.rating,
                                text: "${movieDetailsDTO.voteAverage}",
                              ),
                            ],
                            interItemSpace: 12,
                            flowHorizontal: false,
                          )),
                    ),
                  ),
                ],
                interItemSpace: 12,
              )),
        ),
      ),
    );
  }

  Widget _getDetailView({
    String icon,
    String text,
  }) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: UIUtil.getSeparatedWidgets(
          [
            Image.asset(
              icon,
              width: 16,
              height: 16,
              color: AppColors.white,
            ),
            Text(
              text,
              style: TextStyles.movieCardSubTitleStyle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
          interItemSpace: 8,
        ));
  }

  NetworkImageView _getNetworkImage(String url) {
    return NetworkImageView(
      url,
      placeholderWidget: Container(
        padding: EdgeInsets.all(
          10.0,
        ),
        child: Image.asset(
          Assets.moviePosterPlaceHolder,
          fit: BoxFit.fill,
          height: 120,
          width: 100,
          color: AppColors.bgColor,
        ),
      ),
      imageBuilder: (file) => _getLoadedImage(file),
    );
  }

  Widget _getLoadedImage(File file) {
    if (widgetBuilder == null) {
      return Image.file(
        file,
        fit: BoxFit.fill,
        height: 120,
        width: 100,
      );
    } else {
      return widgetBuilder(file);
    }
  }
}

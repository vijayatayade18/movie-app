import 'dart:io';

import 'package:flutter/material.dart';
import 'package:movie_app/constants/index.dart';
import 'package:movie_app/data/models/index.dart';
import 'package:movie_app/utils/index.dart';
import 'package:movie_app/widgets/network_image_view/index.dart';

class MovieDetailsScreen extends StatefulWidget {
  final MovieDetailsDTO movieDetails;

  const MovieDetailsScreen({Key key, @required this.movieDetails}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MovieDetailsScreenState();
}

class _MovieDetailsScreenState extends State<MovieDetailsScreen> {
  LoadedImageBuilder widgetBuilder;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.bgColor,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Stack(children: [
                Container(
                  child: _getNetworkImage(
                    widget.movieDetails.imagePath,
                  ),
                ),
                Positioned(
                  top: 30,
                  child: IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: AppColors.white,
                    ),
                    onPressed: () {
                      if (Navigator.of(context).canPop()) {
                        Navigator.pop(
                          context,
                        );
                      }
                    },
                  ),
                )
              ]),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: UIUtil.getSeparatedWidgets(
                    [
                      Text(
                        widget.movieDetails.title,
                        style: TextStyles.movieNameStyle,
                      ),
                      Text(
                        widget.movieDetails.overview,
                        style: TextStyles.movieOverviewStyle,
                        textAlign: TextAlign.start,
                        maxLines: 5,
                        overflow: TextOverflow.ellipsis,
                      ),
                      _getDetailView(
                        title: AppStrings.releaseDateLabel,
                        value: widget.movieDetails.releaseDate,
                      ),
                      _getDetailView(
                        title: AppStrings.languageLabel,
                        value: widget.movieDetails.originalLanguage,
                      ),
                      _getDetailView(
                        title: AppStrings.ratingLabel,
                        value: "${widget.movieDetails.voteAverage}",
                      )
                    ],
                    interItemSpace: 20,
                    flowHorizontal: false,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  NetworkImageView _getNetworkImage(String url) {
    return NetworkImageView(
      url,
      placeholderWidget: Image.asset(
        Assets.moviePosterPlaceHolder,
        height: 400,
        fit: BoxFit.fill,
        width: double.infinity,
        color: AppColors.bgColor,
      ),
      imageBuilder: (file) => _getLoadedImage(file),
    );
  }

  Widget _getLoadedImage(File file) {
    if (widgetBuilder == null) {
      return Image.file(
        file,
        height: 400,
        fit: BoxFit.fill,
        width: double.infinity,
      );
    } else {
      return widgetBuilder(file);
    }
  }

  Widget _getDetailView({
    String title,
    String value,
  }) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: UIUtil.getSeparatedWidgets(
          [
            Text(
              title,
              style: TextStyles.movieCardTextStyle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.start,
            ),
            Text(
              value,
              style: TextStyles.movieCardSubTitleStyle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.start,
            ),
          ],
          interItemSpace: 8,
        ));
  }
}

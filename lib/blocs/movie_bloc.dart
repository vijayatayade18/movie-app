import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/constants/index.dart';
import 'package:movie_app/data/models/index.dart';
import 'package:movie_app/data/repo/movies_repo/index.dart';
import 'package:movie_app/screen/movies/events/index.dart';
import 'package:movie_app/screen/movies/states/index.dart';

class MovieBloc extends Bloc<MovieEvents, MovieState> {
  MoviesRepo _moviesRepo = MoviesRepoImpl();
  List<MovieDetailsDTO> moviesList;
  bool isListView;
  ValueNotifier<String> fabButtonIcon = ValueNotifier(null);

  MovieBloc(MovieState initialState) : super(initialState) {
    this.add(LoadingStateEvent());
    isListView = true;
    fabButtonIcon.value = Assets.gridIcon;
  }

  @override
  Stream<MovieState> mapEventToState(event) async* {
    if (event is LoadingStateEvent) {
      yield* fetchMovieList();
    }
    if (event is MoviesListStateEvent) {
      yield MovieListState(moviesList);
    }
    if (event is MoviesGridStateEvent) {
      yield MoviesGridState(moviesList);
    }
  }

  Stream<MovieState> fetchMovieList() async* {
    try {
      moviesList = await _moviesRepo.fetchMoviesList();
      if (moviesList.isNotEmpty) {
        yield MovieListState(moviesList);
      } else {
        yield MovieErrorState(
          errorMessage: AppStrings.noMoviesFound,
          icon: Assets.moviePosterPlaceHolder,
          showRetryButton: false,
        );
      }
    } catch (e) {
      yield MovieErrorState(
        errorMessage: AppStrings.errorMessage,
        icon: Assets.error,
        showRetryButton: true,
        onRetryClicked: () {
          this.add(LoadingStateEvent());
          fetchMovieList();
        },
      );
    }
  }

  void updateUI() {
    if (this.isListView) {
      fabButtonIcon.value = Assets.gridIcon;
      this.add(MoviesListStateEvent());
    } else {
      fabButtonIcon.value = Assets.listIcon;
      this.add(MoviesGridStateEvent());
    }
  }
}

import 'package:movie_app/data/models/index.dart';

abstract class MoviesRepo {
  Future<List<MovieDetailsDTO>> fetchMoviesList();
}

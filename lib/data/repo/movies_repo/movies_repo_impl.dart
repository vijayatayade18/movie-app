import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:movie_app/constants/index.dart';
import 'package:movie_app/data/local/dao/movie_details_dao.dart';
import 'package:movie_app/data/local/db/my_db.dart';
import 'package:movie_app/data/models/index.dart';
import 'package:movie_app/data/network/index.dart';
import 'package:movie_app/data/repo/movies_repo/index.dart';
import 'package:movie_app/utils/index.dart';

class MoviesRepoImpl extends MoviesRepo {
  MyDatabase _myDatabase;

  MoviesRepoImpl() {
    _myDatabase = locator.get();
  }

  @override
  Future<List<MovieDetailsDTO>> fetchMoviesList() async {
    return _fetchMoviesList();
  }

  //api call to fetch movie list
  Future<List<MovieDetailsDTO>> _fetchMoviesList() async {
    List<MovieDetailsDTO> _moviesList = List();
    ApiClient apiClient = ApiClient.public();
    var movieDetailsDao = _myDatabase.movieDetailsDao;

    try {
      Response response = await apiClient.dioClient.get(
        ApiEndPoints.movieListUrl,
      );

      List<MovieDetailsDTO> moviesList = (response.data['results'] as List)?.map((e) => e == null ? null : MovieDetailsDTO.fromJson(e as Map<String, dynamic>))?.toList();

      //store movie list into local db
      await movieDetailsDao.insertMultipleEntries(moviesList);
      _moviesList = await _fetchMovieListFromDB(movieDetailsDao);

      return _moviesList;
    } catch (e) {
      _moviesList = await _fetchMovieListFromDB(movieDetailsDao);
      if (_moviesList.isNotEmpty) {
        return _moviesList;
      } else {
        rethrow;
      }
    }
  }

  //fetch list from local db
  Future<List<MovieDetailsDTO>> _fetchMovieListFromDB(MovieDetailsDao movieDetailsDao) async {
    List<MovieDetailsDTO> _moviesList = List();
    var _moviesListFromDB = await movieDetailsDao.moviesList;
    if (_moviesListFromDB != null && _moviesListFromDB.isNotEmpty) {
      _moviesListFromDB.forEach((element) {
        if (element != null) {
          _moviesList.add(MovieDetailsDTO.fromJson(
            json.decode(element.content),
          ));
        }
      });
    }
    return _moviesList;
  }
}

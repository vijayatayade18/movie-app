import 'package:json_annotation/json_annotation.dart';

part 'movie_details_dto.g.dart';

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class MovieDetailsDTO {
  final int id;
  @JsonKey(name: "original_title")
  final String originalTitle;
  final String overview;
  final double popularity;
  @JsonKey(name: "poster_path")
  final String posterPath;
  @JsonKey(name: "release_date")
  final String releaseDate;
  final String title;
  @JsonKey(name: "vote_average")
  final double voteAverage;
  @JsonKey(name: "vote_count")
  final int voteCount;
  @JsonKey(name: "original_language")
  final String originalLanguage;

  MovieDetailsDTO({
    this.id,
    this.originalTitle,
    this.overview,
    this.popularity,
    this.posterPath,
    this.releaseDate,
    this.title,
    this.voteAverage,
    this.voteCount,
    this.originalLanguage,
  });

  factory MovieDetailsDTO.fromJson(Map<String, dynamic> json) => _$MovieDetailsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$MovieDetailsDTOToJson(this);

  @JsonKey(ignore: true)
  String get imagePath => "https://image.tmdb.org/t/p/original$posterPath";
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_details_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieDetailsDTO _$MovieDetailsDTOFromJson(Map<String, dynamic> json) {
  return MovieDetailsDTO(
    id: json['id'] as int,
    originalTitle: json['original_title'] as String,
    overview: json['overview'] as String,
    popularity: (json['popularity'] as num)?.toDouble(),
    posterPath: json['poster_path'] as String,
    releaseDate: json['release_date'] as String,
    title: json['title'] as String,
    voteAverage: (json['vote_average'] as num)?.toDouble(),
    voteCount: json['vote_count'] as int,
    originalLanguage: json['original_language'] as String,
  );
}

Map<String, dynamic> _$MovieDetailsDTOToJson(MovieDetailsDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'original_title': instance.originalTitle,
      'overview': instance.overview,
      'popularity': instance.popularity,
      'poster_path': instance.posterPath,
      'release_date': instance.releaseDate,
      'title': instance.title,
      'vote_average': instance.voteAverage,
      'vote_count': instance.voteCount,
      'original_language': instance.originalLanguage,
    };

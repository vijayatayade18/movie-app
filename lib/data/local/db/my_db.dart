import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:movie_app/data/local/dao/index.dart';
import 'package:movie_app/data/local/entities/index.dart';

part 'my_db.g.dart';

@UseMoor(
  tables: [
    MovieDetailsEntity,
  ],
  daos: [MovieDetailsDao],
)
class MyDatabase extends _$MyDatabase {
  MyDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
          path: 'db.sqlite',
        ));

  @override
  int get schemaVersion => 1;
}

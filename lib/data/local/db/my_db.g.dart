// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_db.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class MovieDetailsEntityData extends DataClass
    implements Insertable<MovieDetailsEntityData> {
  final int id;
  final String content;
  MovieDetailsEntityData({@required this.id, @required this.content});
  factory MovieDetailsEntityData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return MovieDetailsEntityData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      content:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}content']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || content != null) {
      map['content'] = Variable<String>(content);
    }
    return map;
  }

  MovieDetailsEntityCompanion toCompanion(bool nullToAbsent) {
    return MovieDetailsEntityCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      content: content == null && nullToAbsent
          ? const Value.absent()
          : Value(content),
    );
  }

  factory MovieDetailsEntityData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return MovieDetailsEntityData(
      id: serializer.fromJson<int>(json['id']),
      content: serializer.fromJson<String>(json['content']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'content': serializer.toJson<String>(content),
    };
  }

  MovieDetailsEntityData copyWith({int id, String content}) =>
      MovieDetailsEntityData(
        id: id ?? this.id,
        content: content ?? this.content,
      );
  @override
  String toString() {
    return (StringBuffer('MovieDetailsEntityData(')
          ..write('id: $id, ')
          ..write('content: $content')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode, content.hashCode));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is MovieDetailsEntityData &&
          other.id == this.id &&
          other.content == this.content);
}

class MovieDetailsEntityCompanion
    extends UpdateCompanion<MovieDetailsEntityData> {
  final Value<int> id;
  final Value<String> content;
  const MovieDetailsEntityCompanion({
    this.id = const Value.absent(),
    this.content = const Value.absent(),
  });
  MovieDetailsEntityCompanion.insert({
    this.id = const Value.absent(),
    @required String content,
  }) : content = Value(content);
  static Insertable<MovieDetailsEntityData> custom({
    Expression<int> id,
    Expression<String> content,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (content != null) 'content': content,
    });
  }

  MovieDetailsEntityCompanion copyWith({Value<int> id, Value<String> content}) {
    return MovieDetailsEntityCompanion(
      id: id ?? this.id,
      content: content ?? this.content,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (content.present) {
      map['content'] = Variable<String>(content.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('MovieDetailsEntityCompanion(')
          ..write('id: $id, ')
          ..write('content: $content')
          ..write(')'))
        .toString();
  }
}

class $MovieDetailsEntityTable extends MovieDetailsEntity
    with TableInfo<$MovieDetailsEntityTable, MovieDetailsEntityData> {
  final GeneratedDatabase _db;
  final String _alias;
  $MovieDetailsEntityTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        $customConstraints: 'UNIQUE');
  }

  final VerificationMeta _contentMeta = const VerificationMeta('content');
  GeneratedTextColumn _content;
  @override
  GeneratedTextColumn get content => _content ??= _constructContent();
  GeneratedTextColumn _constructContent() {
    return GeneratedTextColumn(
      'content',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, content];
  @override
  $MovieDetailsEntityTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'movie_details_entity';
  @override
  final String actualTableName = 'movie_details_entity';
  @override
  VerificationContext validateIntegrity(
      Insertable<MovieDetailsEntityData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('content')) {
      context.handle(_contentMeta,
          content.isAcceptableOrUnknown(data['content'], _contentMeta));
    } else if (isInserting) {
      context.missing(_contentMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  MovieDetailsEntityData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return MovieDetailsEntityData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $MovieDetailsEntityTable createAlias(String alias) {
    return $MovieDetailsEntityTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $MovieDetailsEntityTable _movieDetailsEntity;
  $MovieDetailsEntityTable get movieDetailsEntity =>
      _movieDetailsEntity ??= $MovieDetailsEntityTable(this);
  MovieDetailsDao _movieDetailsDao;
  MovieDetailsDao get movieDetailsDao =>
      _movieDetailsDao ??= MovieDetailsDao(this as MyDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [movieDetailsEntity];
}

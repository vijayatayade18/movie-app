// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_details_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$MovieDetailsDaoMixin on DatabaseAccessor<MyDatabase> {
  $MovieDetailsEntityTable get movieDetailsEntity =>
      attachedDatabase.movieDetailsEntity;
}

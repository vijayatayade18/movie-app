import 'dart:convert';

import 'package:moor_flutter/moor_flutter.dart';
import 'package:movie_app/data/local/db/my_db.dart';
import 'package:movie_app/data/local/entities/index.dart';
import 'package:movie_app/data/models/movie_details_dto.dart';

part 'movie_details_dao.g.dart';

@UseDao(tables: [MovieDetailsEntity])
class MovieDetailsDao extends DatabaseAccessor<MyDatabase> with _$MovieDetailsDaoMixin {
  MovieDetailsDao(MyDatabase db) : super(db);

  Future<List<MovieDetailsEntityData>> get moviesList => select(movieDetailsEntity).get();

  Future insertMovieDetails(MovieDetailsEntityData movieDetailsEntityData) => into(movieDetailsEntity).insertOnConflictUpdate(movieDetailsEntityData);

  Future updateOrder(MovieDetailsEntityData movieDetailsEntityData) => update(movieDetailsEntity).replace(movieDetailsEntityData);

  Future<MovieDetailsEntityData> getMovieById(int id) => (select(movieDetailsEntity)..where((t) => t.id.equals(id))).getSingle();

  Future<void> insertMultipleEntries(List<MovieDetailsDTO> list) async {
    List<MovieDetailsEntityData> moviesList = list.map((e) {
      if (e != null) {
        return MovieDetailsEntityData(
          id: e.id,
          content: json.encode(e),
        );
      }
    }).toList();

    await batch((batch) {
      batch.insertAll(
        movieDetailsEntity,
        moviesList,
        mode: InsertMode.insertOrReplace,
      );
    });
  }
}

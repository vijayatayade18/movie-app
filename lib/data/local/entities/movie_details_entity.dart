import 'package:moor_flutter/moor_flutter.dart';

class MovieDetailsEntity extends Table {
  IntColumn get id => integer().customConstraint("UNIQUE")();

  TextColumn get content => text()();

  @override
  Set<Column> get primaryKey => {id};
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app/blocs/index.dart';
import 'package:movie_app/constants/index.dart';
import 'package:movie_app/screen/movies/states/index.dart';
import 'package:movie_app/utils/index.dart';

void main() async {
  await setupLocator();
  runApp(MovieApp());
}

class MovieApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: AppColors.primarySwatch,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        backgroundColor: AppColors.bgColor,
      ),
      home: MovieListPage(),
    );
  }
}

class MovieListPage extends StatefulWidget {
  @override
  _MovieListPageState createState() => _MovieListPageState();
}

class _MovieListPageState extends State<MovieListPage> {
  MovieBloc _movieBloc = MovieBloc(MovieLoadingState());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.bgColor,
      appBar: AppBar(
        title: Text(
          AppStrings.movie,
          style: TextStyles.appBarTitle,
        ),
      ),
      body: BlocProvider(
        create: (BuildContext context) => _movieBloc,
        child: Builder(builder: (context) {
          return BlocBuilder<MovieBloc, MovieState>(
            builder: (context, state) => state as Widget,
          );
        }),
      ),
      floatingActionButton: _getFabButton(),
    );
  }

  Widget _getFabButton() {
    return GestureDetector(
      onTap: () {
        _movieBloc.isListView = !_movieBloc.isListView;
        _movieBloc.updateUI();
      },
      child: Container(
        height: 60,
        width: 60,
        decoration: BoxDecoration(
          color: AppColors.primaryColor,
          shape: BoxShape.circle,
        ),
        padding: EdgeInsets.all(
          16.0,
        ),
        child: ValueListenableBuilder(
          valueListenable: _movieBloc.fabButtonIcon,
          builder: (context, icon, _) {
            return Image.asset(
              icon,
              width: 20,
              height: 20,
              alignment: Alignment.center,
              color: AppColors.white,
            );
          },
        ),
      ),
    );
  }
}
